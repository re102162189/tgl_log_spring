**security**  

- user / password: admin / admin

**log4j2 + sleuth**

```
[2019-09-1914:28:11:550][demo,Kitede-MacBook-Pro.local,ebcf0a601f3ae040,ebcf0a601f3ae040,][INFO]- 
        com.demo.service.EmployeeJdbcService.findById_aroundBody1$advice(EmployeeJdbcService.java:44) - Execution time of EmployeeJdbcService.findById : 471 ms
[2019-09-1914:28:11:551][demo,Kitede-MacBook-Pro.local,ebcf0a601f3ae040,ebcf0a601f3ae040,][INFO]- 
        com.demo.config.ServiceAspect.profileAllMethods(ServiceAspect.java:44) - Execution time of EmployeeJdbcController.findById : 472 ms
```

**mertics**  

- http://localhost:8080/prod/actuator

**prometheus**  

- http://localhost:8080/prod/actuator/prometheus

**swagger ui**  

- http://localhost:8080/prod/swagger-ui.html

**mybatis|JDBC CRUD**  

- select (HTTP method GET): http://localhost:8080/{prod||dev}/rest/emp/{jdbc||mybatis}/{id}  

- insert (HTTP method POST): http://localhost:8080/{prod||dev}/rest/emp/{jdbc||mybatis}  
```
{
    "engName": "kite-chen",
    "email": "kitechen1@tgl.com.tw",
    "ext": 6201
}
```  

- update (HTTP method PUT): http://localhost:8080/{prod||dev}/rest/emp/{jdbc||mybatis}  
```
{
    "engName": "kite-chen",
    "email": "kitechen1@tgl.com.tw",
    "ext": 6201
}
```

- delete (HTTP method DELETE): http://localhost:8080/{prod||dev}/rest/emp/{jdbc||mybatis}/{id}  

- batchinsert (HTTP method POST): http://localhost:8080/{prod||dev}/rest/emp/{jdbc||mybatis}/batchUpdate  

- download (HTTP method GET): http://localhost:8080/{prod||dev}/rest/emp/{jdbc||mybatis}/{id}  