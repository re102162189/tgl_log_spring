package com.demo.app;

import static org.junit.Assert.assertTrue;
import org.jasypt.encryption.StringEncryptor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DataSourceTest {

    @Autowired
    private StringEncryptor stringEncryptor;
    
    @Test
    public void encodePassword() {
        String password = "admin";
        String encryption = stringEncryptor.encrypt(password);
        String decryption = stringEncryptor.decrypt(encryption);
        
        System.out.println("===========================================");  
        System.out.println("encryption: " + encryption);
        System.out.println("decryption: " + decryption);
        System.out.println("===========================================");  
        assertTrue(password.equals(decryption));

        String bcrypted = new BCryptPasswordEncoder().encode(password);
        String bcryptedEncryption = stringEncryptor.encrypt(bcrypted);
        String bcryptedDecryption = stringEncryptor.decrypt(bcryptedEncryption);
        
        System.out.println("===========================================");  
        System.out.println("bcrypted: " + bcrypted);
        System.out.println("===========================================");  
        System.out.println("bcryptedEncryption: " + bcryptedEncryption);
        System.out.println("===========================================");  
        System.out.println("bcryptedDecryption: " + bcryptedDecryption);
        System.out.println("===========================================");       
        assertTrue(bcrypted.equals(bcryptedDecryption));
    }
    
    
}
