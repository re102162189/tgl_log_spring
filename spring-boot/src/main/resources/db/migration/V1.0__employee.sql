/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE TABLE `employee` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `eng_name` varchar(50) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `ext` smallint(4) unsigned NOT NULL DEFAULT '0',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;

INSERT INTO `employee` (`id`, `eng_name`, `email`, `ext`, `create_time`, `update_time`)
VALUES
	(1,'kite-chen','kitechen1@tgl.com.tw',6201,'2019-09-12 07:50:42','2019-09-12 07:50:42'),
	(2,'rock-lin','rocklin1@tgl.com.tw',6202,'2019-09-12 07:50:42','2019-09-12 07:50:42'),
	(3,'kite-chenn','kitechen2@tgl.com.tw',6203,'2019-09-12 07:50:42','2019-09-12 07:50:42'),
	(4,'rock-linn','rocklin@t2gl.com.tw',6204,'2019-09-12 07:50:42','2019-09-12 07:50:42'),
	(5,'kitee-chen','kitechen3@tgl.com.tw',6205,'2019-09-12 07:50:42','2019-09-12 07:50:42'),
	(6,'rockk-lin','rocklin3@tgl.com.tw',6206,'2019-09-12 07:50:42','2019-09-12 07:50:42'),
	(7,'kitee-chenn','kitechen4@tgl.com.tw',6207,'2019-09-12 07:50:42','2019-09-12 07:50:42'),
	(8,'rockk-linn','rocklin4@tgl.com.tw',6208,'2019-09-12 07:50:42','2019-09-12 07:50:42'),
	(9,'kit-chen','kitechen5@tgl.com.tw',6209,'2019-09-12 07:50:42','2019-09-12 07:50:42'),
	(10,'roc-lin','rocklin5@tgl.com.tw',6210,'2019-09-12 07:50:42','2019-09-12 07:50:42'),
	(11,'kite-che','kitechen6@tgl.com.tw',6211,'2019-09-12 07:50:42','2019-09-12 07:50:42'),
	(12,'rock-li','rocklin6@tgl.com.tw',6212,'2019-09-12 07:50:42','2019-09-12 07:50:42'),
	(13,'kit-che','kitechen7@tgl.com.tw',6213,'2019-09-12 07:50:42','2019-09-12 07:50:42'),
	(14,'roc-li','rocklin7@tgl.com.tw',6214,'2019-09-12 07:50:42','2019-09-12 07:50:42'),
	(15,'kie-chen','kitechen8@tgl.com.tw',6215,'2019-09-12 07:50:42','2019-09-12 07:50:42'),
	(16,'rok-lin','rocklin8@tgl.com.tw',6216,'2019-09-12 07:50:42','2019-09-12 07:50:42');

/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
