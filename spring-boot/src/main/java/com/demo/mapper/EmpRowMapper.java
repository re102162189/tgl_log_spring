package com.demo.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.demo.bean.Employee;

public class EmpRowMapper implements RowMapper<Employee> {
  public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {
    Employee emp = new Employee();
    emp.setId(rs.getLong("id"));
    emp.setEngName(rs.getString("eng_name"));
    emp.setEmail(rs.getString("email"));
    emp.setExt(rs.getInt("ext"));
    return emp;
  }
}
