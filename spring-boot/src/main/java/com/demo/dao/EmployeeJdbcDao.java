package com.demo.dao;

import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.demo.bean.Employee;
import com.demo.mapper.EmpRowMapper;

@Repository
@Qualifier("employeeJdbcDao")
public class EmployeeJdbcDao {

  @Autowired
  private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

  private static final String FIND_ALL = "SELECT id, eng_name, email, ext FROM employee";

  private static final String FIND_BY_ID =
      "SELECT id, eng_name, email, ext FROM employee WHERE id=:id";

  private static final String INSERT =
      "INSERT INTO employee (eng_name, email, ext) VALUES (:engName, :email, :ext)";

  private static final String UPDATE =
      "UPDATE employee SET eng_name=:engName, email=:email, ext=:ext WHERE id=:id";

  private static final String DELETE_BY_ID = "DELETE FROM employee WHERE id=:id";

  @Cacheable(value = "empFindById", key = "#id")
  public Employee findById(long id) {
    return namedParameterJdbcTemplate.queryForObject(FIND_BY_ID,
        new MapSqlParameterSource("id", id), new EmpRowMapper());
  }

  public int insert(Employee emp) {
    return namedParameterJdbcTemplate.update(INSERT, new BeanPropertySqlParameterSource(emp));
  }

  @CachePut(value = "empFindById", key = "#emp.id")
  public int update(Employee emp) {
    return namedParameterJdbcTemplate.update(UPDATE, new BeanPropertySqlParameterSource(emp));
  }

  @CacheEvict(value = "empFindById", key = "#id")
  public int delete(long id) {
    return namedParameterJdbcTemplate.update(DELETE_BY_ID, new MapSqlParameterSource("id", id));
  }

  @CacheEvict(value = "empFindById")
  @Transactional(propagation = Propagation.REQUIRED, rollbackFor = SQLException.class)
  public int batchUpdate(List<Employee> list) {
    SqlParameterSource[] batch = SqlParameterSourceUtils.createBatch(list.toArray());
    int[] updateCounts = namedParameterJdbcTemplate.batchUpdate(INSERT, batch);
    return updateCounts.length;
  }

  public List<Employee> findAll() {
    return namedParameterJdbcTemplate.query(FIND_ALL, new EmpRowMapper());
  }
}
