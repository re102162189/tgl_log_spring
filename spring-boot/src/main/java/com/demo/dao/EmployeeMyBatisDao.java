package com.demo.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.InsertProvider;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.demo.bean.Employee;

public interface EmployeeMyBatisDao {

  Employee findById(long id);

  int insert(Employee user);

  int update(Employee user);

  int delete(long id);

  @InsertProvider(type = Provider.class, method = "batchUpdate")
  @Transactional(propagation = Propagation.REQUIRED, rollbackFor = SQLException.class)
  boolean batchUpdate(List<Employee> list);
  
  List<Employee> findAll();
  
  class Provider {
    public String batchUpdate(Map<String, List<Employee>> map) {
      List<Employee> items = map.get("list");
      StringBuilder sb = new StringBuilder();
      sb.append("INSERT INTO employee (eng_name, email, ext) VALUES ");

      for (int i = 0; i < items.size(); i++) {
        Employee emp = items.get(i);
        sb.append(String.format("(%s, %s, %s)", emp.getEngName(), emp.getEmail(), emp.getExt()));
        if (i < items.size() - 1) {
          sb.append(",");
        }
      }
      return sb.toString();
    }
  }
}
