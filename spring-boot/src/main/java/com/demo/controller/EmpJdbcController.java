package com.demo.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.demo.bean.Employee;
import com.demo.service.IGerenicService;
import com.tgl.logger.common.LogTag;

@RestController
@RequestMapping(value = "/rest/emp/jdbc")
public class EmpJdbcController extends AEmpController {
  
  @Autowired
  EmpJdbcController(@Qualifier("empJdbcService") IGerenicService<Employee> empService) {
    this.empService = empService;
  }
  
  @LogTag(catalog = "findById")
  @GetMapping(value = "/{id}")
  public Employee findById(@PathVariable("id") long id) {
    return empService.findById(id);
  }

  @PostMapping
  public boolean insert(@Valid @RequestBody Employee emp) {
    return empService.insert(emp);
  }

  @PutMapping
  public boolean update(@Valid @RequestBody Employee emp) {
    return empService.update(emp);
  }

  @DeleteMapping(value = "/{id}")
  public boolean delete(@PathVariable("id") long id) {
    return empService.delete(id);
  }
}
