package com.demo.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import com.demo.bean.Employee;
import com.demo.service.IGerenicService;

public abstract class AEmpController {
  
  private static final Logger logger = LogManager.getLogger(AEmpController.class);
  
  protected IGerenicService<Employee> empService;
  
  @GetMapping(value = "/home")
  public String home() {
    return "home";
  }
  
  @PostMapping("/batchUpdate")
  public boolean batchUpdate(@RequestParam("file") MultipartFile file,
      @RequestHeader Map<String, String> headers) {

    if (file.isEmpty()) {
      return false;
    }

    headers.forEach((key, value) -> {
      logger.debug(String.format("Header '%s' = %s", key, value));
    });

    List<String> rawList = new ArrayList<>();

    try (BufferedReader bfReader =
        new BufferedReader(new InputStreamReader(file.getInputStream(), StandardCharsets.UTF_8))) {
      String line = null;
      while ((line = bfReader.readLine()) != null) {
        rawList.add(line);
      }
    } catch (IOException e) {
      logger.error("file: {}, error: {}", file.getName(), e);
    }

    return empService.batchUpdateRaw(rawList);
  }

  @GetMapping(value = "/download")
  public @ResponseBody HttpEntity<byte[]> download() {
    String empList = empService.download();
    byte[] document = empList.getBytes();
    HttpHeaders header = new HttpHeaders();
    header.setContentType(new MediaType("application", "csv"));
    header.set("Content-Disposition", "inline; filename=emp.csv");
    header.setContentLength(document.length);
    return new HttpEntity<>(document, header);
  }
}
