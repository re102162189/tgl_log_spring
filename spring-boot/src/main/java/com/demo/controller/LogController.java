package com.demo.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.demo.service.LogService;

@RestController
@RequestMapping(value = "/rest/log")
public class LogController {

  private LogService logService;

  @Autowired
  LogController(@Qualifier("logService") LogService logService) {
    this.logService = logService;
  }

  @GetMapping(value = "/oom")
  public void error1() {
    List<Integer> list = new ArrayList<>();
    for (int i = 0; i < Integer.MAX_VALUE; i++) {
      list.add(i); 
    }
    // runtime unchecked exception: out of memory
  }

  @GetMapping(value = "/npe")
  public void error2() {
    int[] i = null;
    int ib = i[0];
    // runtime unchecked exception: null pointer
  }

  @GetMapping(value = "/arithmetic")
  public void error3() {
    double d = 3 / 0;
    
    // runtime unchecked exception: arithmetic
  }

  @GetMapping(value = "/logRequired")
  public String logRequired() {
    logService.logRequired();
    return "logRequired";
  }
  
  @GetMapping(value = "/logExtFields")
  public String logExtFields() {
    logService.logExtFields();
    return "logExtFields";
  }
  
  @GetMapping(value = "/logOptional")
  public String logOptional() {
    logService.logOptional();
    return "logOptional";
  }

}
