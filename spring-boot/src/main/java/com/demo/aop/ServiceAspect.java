package com.demo.aop;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.CodeSignature;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.MDC;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import com.tgl.logger.common.LogConstant;
import com.tgl.logger.common.LogTag;

@Aspect
public class ServiceAspect {

  @Pointcut("execution(* com.demo.service.*.*(..))")
  public void serviceLayer() {}

  @Pointcut("@annotation(com.tgl.logger.common.LogTag)")
  public void logTag() {}

  /**
   * @param joinPoint
   */
  @Before("serviceLayer() || logTag()")
  public void serviceBefore(JoinPoint joinPoint) {

    SecurityContext securityContext = SecurityContextHolder.getContext();
    Authentication authentication = securityContext.getAuthentication();
    String userName = null;
    String roles = null;
    if (authentication != null) {
      UserDetails userDetails = (UserDetails) authentication.getPrincipal();
      userName = userDetails.getUsername();
      Set<String> rolesSet = new HashSet<>();
      Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
      for (GrantedAuthority grantedAuthority : authorities) {
        rolesSet.add(grantedAuthority.getAuthority());
      }
      roles = String.join(",", rolesSet);
    }

    String className =
        joinPoint.getTarget().getClass().getName() + "." + joinPoint.getSignature().getName();

    MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
    Method method = methodSignature.getMethod();
    LogTag logtag = method.getAnnotation(LogTag.class);
    if (logtag != null) {
      MDC.put(LogConstant.CATALOG, logtag.catalog());
    }

    CodeSignature codeSignature = (CodeSignature) joinPoint.getSignature();
    for (int i = 0; i < codeSignature.getParameterNames().length; i++) {
      MDC.put(codeSignature.getParameterNames()[i], joinPoint.getArgs()[i].toString());
    }

    MDC.put(LogConstant.CLASSNAME, className);
    MDC.put(LogConstant.USERNANE, userName);
    MDC.put(LogConstant.USER_ROLES, roles);
  }
}
