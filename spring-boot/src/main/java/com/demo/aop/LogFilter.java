package com.demo.aop;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Component;
import com.tgl.logger.common.LogConstant;
import com.tgl.logger.common.TglLogger;
import com.tgl.logger.common.TglLoggerFactory;

@Component
public class LogFilter implements Filter {

  private static final TglLogger logger = TglLoggerFactory.getLogger("e2e");

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterchain)
      throws IOException, ServletException {

    HttpServletRequest httprequest = ((HttpServletRequest) request);
    HttpSession session = httprequest.getSession();

    logger.base()
        .setUTID(httprequest.getHeader(LogConstant.UTID))
        .setSessionId(session.getId())
        .setHost(httprequest.getHeader("Host"))
        .setRemoteHost(httprequest.getRemoteHost())
        .setRemoteAddress(httprequest.getRemoteAddr());

    try {
      filterchain.doFilter(request, response);
    } catch (RuntimeException ex) {
      logger.error().log("Uncaught RuntimeException", ex);
    }
    
  }

  @Override
  public void init(FilterConfig filterconfig) throws ServletException {}
  
  @Override
  public void destroy() {}
}
