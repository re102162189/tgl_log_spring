package com.demo.bean;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class Employee {

  private long id;

  @NotNull
  @Pattern(regexp = "^[a-zA-Z0-9]{2,32}-[a-zA-Z0-9]{2,32}$")
  private String engName;

  @NotNull
  @Email
  private String email;

  @NotNull
  @Digits(integer = 4, fraction = 0)
  private int ext;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getEngName() {
    return engName;
  }

  public void setEngName(String engName) {
    this.engName = engName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public int getExt() {
    return ext;
  }

  public void setExt(int ext) {
    this.ext = ext;
  }
}
