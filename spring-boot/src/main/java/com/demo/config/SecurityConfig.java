package com.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
  
  @Override
  public void configure(WebSecurity web) throws Exception {
      web.ignoring()
          .antMatchers("/resources/**");
  }
  
  @Configuration
  @Order(1)
  public static class ApiWebSecurityConfig extends WebSecurityConfigurerAdapter{
      
      @Value("${actuator.username}")
      private String username;
  
      @Value("${actuator.password}")
      private String password;
  
      @Value("${actuator.role}")
      private String role;
      
      @Override
      protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
        .withUser(username)
        .password(password)
        .roles(role);
      }
      
      @Override
      protected void configure(HttpSecurity http) throws Exception {
          http
              .requestMatchers()
              .antMatchers("/rest/**", "/actuator/**")
            .and()
              .authorizeRequests().anyRequest().hasRole(role)
            .and()
              .httpBasic()
            .and()
              .sessionManagement()
              .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
              .sessionFixation()
              .migrateSession()
            .and()
              .csrf().disable()
              .cors().disable();
     }
  }
  
  @Configuration
  @Order(2)
  public static class ViewWebSecurityConfig extends WebSecurityConfigurerAdapter{
      
      @Autowired
      public UserService userService;
      
      @Override
      protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService);
      }
    
      @Override
      protected void configure(HttpSecurity http) throws Exception {
          http
              .antMatcher("/view/**")
              .authorizeRequests().anyRequest().hasRole("ADMIN")
            .and()  
              .formLogin()
                .loginPage("/login")
                .loginProcessingUrl("/login")
                .defaultSuccessUrl("/view/hello")
                .failureUrl("/login?error=error")
                .permitAll()
            .and()
              .logout()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/login?logout")
                .invalidateHttpSession(true)
                .clearAuthentication(true)
                .deleteCookies("JSESSIONID")
                .permitAll()
            .and()
                .exceptionHandling()
                .accessDeniedPage("/403")    
            .and()
              .csrf().ignoringAntMatchers("/login", "/logout")
            .and()
              .cors().disable();
     }
  }

  @Bean
  public PasswordEncoder encoder() {
    return new BCryptPasswordEncoder();
  }
}
