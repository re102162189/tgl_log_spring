package com.demo.config;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserDetailsService {

  @Value("${actuator.username}")
  private String username;

  @Value("${actuator.password}")
  private String password;

  @Value("${actuator.role}")
  private String role;

  @Override
  public UserDetails loadUserByUsername(String username) {
    List<UserDetails> userDetailsList = populateUserDetails();
    for (UserDetails u : userDetailsList) {
      if (u.getUsername().equals(username)) {
        return u;
      }
    }
    throw new UsernameNotFoundException(username);
  }
  
  /**
   * mock DB
   * @return
   */
  public List<UserDetails> populateUserDetails() {
    List<UserDetails> userDetailsList = new ArrayList<>();
    userDetailsList.add(User.withUsername(username)
                            .password(password)
                            .roles(role).build());
    return userDetailsList;
  }
}
