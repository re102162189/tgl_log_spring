package com.demo.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.demo.bean.Employee;
import com.demo.dao.EmployeeJdbcDao;

@Service
@Qualifier("empJdbcService")
public class EmpJdbcService extends AEmpService implements IGerenicService<Employee> {
  
  @Autowired
  EmployeeJdbcDao employeeJdbcDao;

  public Employee findById(long id) {
    return employeeJdbcDao.findById(id);
  }

  public boolean insert(Employee emp) {
    return employeeJdbcDao.insert(emp) > 0;
  }

  public boolean update(Employee emp) {
    return employeeJdbcDao.update(emp) > 0;
  }

  public boolean delete(long id) {
    return employeeJdbcDao.delete(id) > 0;
  }

  public boolean batchUpdate(List<Employee> list) {
    return employeeJdbcDao.batchUpdate(list) > 0;
  }
  
  public String download() {
    return this.convertCSV(employeeJdbcDao.findAll());
  }
}
