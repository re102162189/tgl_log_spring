package com.demo.service;

import java.util.List;

public interface IGerenicService <T> {
  
  T findById(long id);
  
  boolean insert(T t);
  
  boolean update(T t);
  
  boolean delete(long id);
  
  boolean batchUpdate(List<T> list);
  
  boolean batchUpdateRaw(List<String> list);
  
  String download();
}
