package com.demo.service;

import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.tgl.logger.common.TglLogger;
import com.tgl.logger.common.TglLoggerFactory;

@Service
@Qualifier("logService")
public class LogService {

  private static final TglLogger tgllogger = TglLoggerFactory.getLogger("e2e");
  
  private static final Logger logger = LoggerFactory.getLogger(LogService.class);

  public void logRequired() {
    tgllogger.e2e()
          .operateFunc("log")
          .messageID("log id")
          .messageContent("msg content")
          .log();
    
    logger.info("asdsadsads");
    
  }

  public void logExtFields() {
    Map<String, String> extfields = new HashMap<>();
    extfields.put("op1key", "op1val");

    tgllogger.e2e()
          .operateFunc("log")
          .messageID("log id")
          .messageContent("msg content")
          .log(extfields, "mdg");
  }

  public void logOptional() {
    Map<String, String> extfields = new HashMap<>();
    extfields.put("op1key", "op1val");

    tgllogger.e2e()
          .operateFunc("log")
          .messageID("log id")
          .messageContent("msg content")
          .rq("123")
          .rs("456").log("msg");
  }
}
