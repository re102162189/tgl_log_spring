package com.demo.service;

import java.util.ArrayList;
import java.util.List;
import com.demo.bean.Employee;

public abstract class AEmpService {

  public boolean batchUpdateRaw(List<String> rawList) {
    List<Employee> empList = new ArrayList<>();
    for (String raw : rawList) {
      String[] data = raw.split(",");
      if (data == null || data.length != 3) {
        continue;
      }

      Employee emp = new Employee();
      emp.setEngName(data[0]);
      emp.setEmail(data[1]);
      emp.setExt(Integer.parseInt(data[2]));
      empList.add(emp);
    }
    return batchUpdate(empList);
  }

  public String convertCSV(List<Employee> list) {
    StringBuilder sb = new StringBuilder();
    for (Employee emp : list) {
      sb.append(emp.getId()).append(",");
      sb.append(emp.getEngName()).append(",");
      sb.append(emp.getEmail()).append(",");
      sb.append(emp.getExt()).append("\n");
    }
    return sb.toString();
  }

  public abstract boolean batchUpdate(List<Employee> list);
}
