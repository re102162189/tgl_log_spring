package com.demo.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.demo.bean.Employee;
import com.demo.dao.EmployeeMyBatisDao;

@Service
@Qualifier("empMyBatisService")
public class EmpMyBatisService extends AEmpService implements IGerenicService<Employee> {
  
  @Autowired
  EmployeeMyBatisDao empDao;

  public Employee findById(long id) {
    return empDao.findById(id);
  }

  public boolean insert(Employee emp) {
    return empDao.insert(emp) > 0;
  }

  public boolean update(Employee emp) {
    return empDao.update(emp) > 0;
  }

  public boolean delete(long id) {
    return empDao.delete(id) > 0;
  }

  public boolean batchUpdate(List<Employee> list) {
    return empDao.batchUpdate(list);
  }
  
  public String download() {
    return this.convertCSV(empDao.findAll());
  }
}
