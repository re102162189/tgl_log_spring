package com.demo.aop;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import com.tgl.logger.common.LogConstant;
import com.tgl.logger.common.TglLogger;
import com.tgl.logger.common.TglLoggerFactory;

public class LogFilter implements Filter {

  private static final TglLogger logger = TglLoggerFactory.getLogger(LogFilter.class);
  
  private List<String> excludedUrls;

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterchain)
      throws IOException, ServletException {

    HttpServletRequest httprequest = ((HttpServletRequest) request); 
    String path = httprequest.getServletPath();
    
    if(!this.isExcluded(path)) {
      logger.base()
        .setUTID(httprequest.getHeader(LogConstant.UTID))
        .setSessionId(httprequest.getSession().getId())
        .setHost(httprequest.getHeader(LogConstant.HOST))
        .setRemoteHost(httprequest.getRemoteHost())
        .setRemoteAddress(httprequest.getRemoteAddr()); 
    }
    
    try {
      filterchain.doFilter(request, response);
    } catch (RuntimeException ex) {
      logger.error().log("Uncaught RuntimeException", ex);
    }
  }

  @Override
  public void init(FilterConfig filterconfig) throws ServletException {
    String excludedPatterns = filterconfig.getInitParameter("excludedUrls");
    excludedUrls = Arrays.asList(excludedPatterns.split(","));
  }
  
  private boolean isExcluded(String path) {
    for(String excludedUrl : excludedUrls) {
      if(excludedUrl.contains(path)) {
        return true;
      }
    }
    return false;
  }
  
  @Override
  public void destroy() {}
}
