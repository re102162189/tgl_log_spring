package com.demo.aop;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.tgl.logger.common.TglLogger;
import com.tgl.logger.common.TglLoggerFactory;


@WebServlet("/errorHandler")
public class ErrorHandler extends HttpServlet {
  
  private static final long serialVersionUID = 1L;
  
  private static final TglLogger logger = TglLoggerFactory.getLogger(ErrorHandler.class);

  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    Throwable throwable = (Throwable) request.getAttribute("javax.servlet.error.exception"); 
    logger.error().log("Uncaught Throwable Error", throwable);
  }

  @Override
  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    doGet(request, response);
  }
}
