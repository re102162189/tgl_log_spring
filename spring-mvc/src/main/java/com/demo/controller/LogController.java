package com.demo.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.tgl.logger.common.TglLogger;
import com.tgl.logger.common.TglLoggerFactory;

@RestController
@RequestMapping(value = "/rest/log")
public class LogController {

  private static final TglLogger logger = TglLoggerFactory.getLogger(LogController.class);

  @GetMapping(value = "/oom")
  public void error1() {
    List<Integer> list = new ArrayList<>();
    for (int i = 0; i < Integer.MAX_VALUE; i++) {
      list.add(i);
    }
    // runtime unchecked exception: out of memory
  }

  @GetMapping(value = "/npe")
  public void error2() {
    logger.e2e().operateFunc("error2").messageID("msgID").messageContent("msgcont").log();
    int[] i = null;
    int ib = i[0];
    // runtime unchecked exception: null pointer
  }

  @GetMapping(value = "/arithmetic")
  public void error3() {
    double d = 3 / 0;
    // runtime unchecked exception: arithmetic
  }

}
