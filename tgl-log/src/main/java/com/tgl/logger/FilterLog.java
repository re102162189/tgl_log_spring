package com.tgl.logger;

import java.util.UUID;
import org.slf4j.MDC;
import com.tgl.logger.common.LogConstant;

public final class FilterLog implements IFilterLog.UTID, IFilterLog.SessionId, IFilterLog.Host,
    IFilterLog.RemoteHost, IFilterLog.RemoteAddress  {
  
  private static final String SESSION_ID = "session_id";
  
  private static final String HOST = "host";
  
  private static final String REMOTE_HOST = "remote_host";
  
  private static final String REMOTE_ADDR = "remote_address";
  
  private FilterLog() {}

  public static IFilterLog.UTID base() {
    return new FilterLog();
  }

  @Override
  public IFilterLog.SessionId setUTID(String utId) {
    if (utId == null || "".equals(utId)) {
      MDC.put(LogConstant.UTID, UUID.randomUUID().toString());
    } else {
      MDC.put(LogConstant.UTID, utId);
    }
    return this;
  }

  @Override
  public IFilterLog.Host setSessionId(String val) {
    MDC.put(SESSION_ID, val);
    return this;
  }

  @Override
  public IFilterLog.RemoteHost setHost(String val) {
    MDC.put(HOST, val);
    return this;
  }

  @Override
  public IFilterLog.RemoteAddress setRemoteHost(String val) {
    MDC.put(REMOTE_HOST, val);
    return this;
  }

  @Override
  public void setRemoteAddress(String val) {
    MDC.put(REMOTE_ADDR, val);
  }
}
