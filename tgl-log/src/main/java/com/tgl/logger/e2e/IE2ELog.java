package com.tgl.logger.e2e;

import java.util.Map;
import com.tgl.logger.ILog;

public interface IE2ELog extends ILog.Log {

  public interface OperateFunc {
    MessageID operateFunc(String val);
  }

  public interface MessageID {
    MessageContent messageID(String val);
  }

  public interface MessageContent {
    IE2ELog.E2E messageContent(String val);
  }

  public interface E2E {
    
    IE2ELog.E2E rq(String val);
    
    IE2ELog.E2E rs(String val);
    
    void log(Map<String, String> optionals, String msg);
    
    void log(String msg);
    
    void log();
  }

}
