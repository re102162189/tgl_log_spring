package com.tgl.logger.e2e;

import java.util.Map;
import org.slf4j.Logger;
import com.tgl.logger.ALog;

public final class E2ELog extends ALog
    implements IE2ELog.OperateFunc, IE2ELog.MessageID, IE2ELog.MessageContent, IE2ELog.E2E {

  private static final String OPERATA_FUNC = "operate_func";
  private static final String MSG_ID = "message_id";
  private static final String MSG_CONT = "message_content";

  private static final String RQ = "rq";
  private static final String RS = "rs";

  private E2ELog(Logger logger) {
    super.init(logger);
  }

  public static IE2ELog.OperateFunc e2e(Logger logger) {
    return new E2ELog(logger);
  }

  @Override
  public IE2ELog.MessageID operateFunc(String val) {
    requiredMap.put(OPERATA_FUNC, val);
    return this;
  }

  @Override
  public IE2ELog.MessageContent messageID(String val) {
    requiredMap.put(MSG_ID, val);
    return this;
  }

  @Override
  public IE2ELog.E2E messageContent(String val) {
    requiredMap.put(MSG_CONT, val);
    return this;
  }

  @Override
  public IE2ELog.E2E rq(String val) {
    optionalMap.put(RQ, val);
    return this;
  }

  @Override
  public IE2ELog.E2E rs(String val) {
    optionalMap.put(RS, val);
    return this;
  }
  
  @Override
  public void log(Map<String, String> optionals, String msg) {
    super.log(optionals);
    this.logger.info(msg);
    this.clearMDC();
  }
  
  @Override
  public void log(String msg) {
    super.log();
    this.logger.info(msg);
    this.clearMDC();
  }

  @Override
  public void log() {
    super.log();
    this.logger.info("e2e log built");
    this.clearMDC();
  }

}
