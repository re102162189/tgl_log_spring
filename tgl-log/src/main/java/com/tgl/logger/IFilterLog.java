package com.tgl.logger;

public interface IFilterLog {

  public interface UTID {
    SessionId setUTID(String vals);
  }

  public interface SessionId {
    Host setSessionId(String val);
  }

  public interface Host {
    RemoteHost setHost(String val);
  }

  public interface RemoteHost {
    RemoteAddress setRemoteHost(String val);
  }

  public interface RemoteAddress {
    void setRemoteAddress(String val);
  }
  
}
