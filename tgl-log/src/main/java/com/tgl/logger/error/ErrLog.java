package com.tgl.logger.error;

import java.util.Map;
import org.slf4j.Logger;
import com.tgl.logger.ALog;

public final class ErrLog extends ALog implements IErrLog.Error {
  
  private ErrLog(Logger logger) {
    super.init(logger);
  }

  public static IErrLog.Error error(Logger logger) {
    return new ErrLog(logger);
  }
  
  @Override
  public void log(Map<String, String> extentals, Throwable t) {
    super.log(extentals);
    this.logger.error("error log built", t);
    this.clearMDC();
  }
  
  @Override
  public void log(String msg, Throwable t) {
    super.log();
    this.logger.error(msg, t);
    this.clearMDC();
  }
}
