package com.tgl.logger.error;

import java.util.Map;
import com.tgl.logger.ILog;

public interface IErrLog extends ILog {
  
  public interface Error {
    
    void log(Map<String, String> externals, Throwable t);
    
    void log(String msg, Throwable t);
  }

}
