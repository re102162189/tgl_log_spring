package com.tgl.logger;

import java.util.HashMap;
import java.util.Map;
import org.slf4j.MDC;
import org.slf4j.Logger;

public abstract class ALog implements ILog.Log {

  protected Logger logger;

  protected Map<String, String> requiredMap;
  
  protected Map<String, String> optionalMap;
  
  protected void init(Logger logger) {
    this.logger = logger;
    requiredMap = new HashMap<>();
    optionalMap = new HashMap<>();
  }
  
  public void log(Map<String, String> extentals) {
    for (Map.Entry<String, String> required : extentals.entrySet()) {
      MDC.put(required.getKey(), required.getValue());
    }
    this.log();
  }
  
  public void log() {
    for (Map.Entry<String, String> params : requiredMap.entrySet()) {
      MDC.put(params.getKey(), params.getValue());
    }
    for (Map.Entry<String, String> params : optionalMap.entrySet()) {
      MDC.put(params.getKey(), params.getValue());
    }
  }
  
  protected void clearMDC() {
    for (Map.Entry<String, String> params : requiredMap.entrySet()) {
      MDC.remove(params.getKey());
    }
    for (Map.Entry<String, String> params : optionalMap.entrySet()) {
      MDC.remove(params.getKey());
    }
  }
}
