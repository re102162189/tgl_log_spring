package com.tgl.logger.common;

import org.slf4j.Logger;
import com.tgl.logger.FilterLog;
import com.tgl.logger.IFilterLog;
import com.tgl.logger.e2e.E2ELog;
import com.tgl.logger.e2e.IE2ELog;
import com.tgl.logger.error.ErrLog;
import com.tgl.logger.error.IErrLog;

public final class TglLogger {

  private Logger logger;

  private TglLogger(Logger logger) {
    this.logger = logger;
  }

  public static TglLogger build(Logger logger) {
    return new TglLogger(logger);
  }

  public IFilterLog.UTID base() {
    return FilterLog.base();
  }

  public IE2ELog.OperateFunc e2e() {
    return E2ELog.e2e(this.logger);
  }

  public IErrLog.Error error() {
    return ErrLog.error(this.logger);
  }

}
