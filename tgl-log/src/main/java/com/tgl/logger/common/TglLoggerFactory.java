package com.tgl.logger.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class TglLoggerFactory {
  
  private TglLoggerFactory() {}

  public static TglLogger getLogger(Class<?> clazz) {
    Logger logger = LoggerFactory.getLogger(clazz);
    return TglLogger.build(logger);
  }
  
  public static TglLogger getLogger(String name) {
    Logger logger = LoggerFactory.getLogger(name);
    return TglLogger.build(logger);
  }
}
