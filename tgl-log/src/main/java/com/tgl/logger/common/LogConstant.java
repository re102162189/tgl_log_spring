package com.tgl.logger.common;

public final class LogConstant {
  
  public static final String UTID = "utid";
  
  public static final String HOST = "Host";
  
  public static final String CLASSNAME = "classname";
  
  public static final String USERNANE = "uername";
  
  public static final String USER_ROLES = "user_roles";
  
  public static final String CATALOG = "catalog";
  
  private LogConstant() {}
}
