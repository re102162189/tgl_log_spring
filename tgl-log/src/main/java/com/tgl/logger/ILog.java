package com.tgl.logger;

import java.util.Map;

public interface ILog {

  public interface Log {
    void log(Map<String, String> optionals);
    
    void log();
  }
  
}
